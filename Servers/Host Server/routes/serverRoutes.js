var app = require('express');
var router = app.Router();

router.get('/', (req, res, next) => {
    res.send("Server Route");
});

router.post('/register', (req, res, next) => {
    res.send("Registered");
});

module.exports = router;