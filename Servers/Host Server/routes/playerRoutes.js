var app = require("express");
var router = app.Router();
const config = require("../config");
const axios = require("axios");
var players = require("../nodeLists");
const communicationManager = require("../communicationManager");
const { sendLobbyStart } = require("../communicationManager");
const { lobbyPlayerCount } = require("../config");

router.get("/", (req, res, next) => {
  res.send("Client Route");
});

router.post("/register", (req, res, next) => {
  var ip = req.headers["x-forwarded-for"] || req.socket.remoteAddress;
  var port = req.body.port;

  let player = {
    ip: ip,
    port: port,
    hp: 100,
    range: 20,
    location: {
      x: 0,
      y: 0,
    },
  };

  if (players.addNode(player) == true) {
    console.log(`Player (${player.ip}:${player.port}) joined the lobby`);
    // communicationManager.sendLobbyUpdateMessage();
    // setTimeout(() => { communicationManager.sendLobbyStart(); }, 500);
    res.send(`200`);

  } else {
    res.send(`500`);
  }

  if(players.map.size == lobbyPlayerCount){
    communicationManager.sendLobbyStart();
  }
});

module.exports = router;
