const { default: axios } = require("axios");
const { playerServerPort } = require("./config");

/**
 * Each node should have atleast .ip && .port
 */
class NodeGroup {
    constructor() {
        // console.log('NodeSingleton Instance created 🎈');
        this.map  = new Map();

        setInterval(() => {
            this.checkGroupHealth();
        }, 5000)
    }

    addNode(node){
        if(this.exists(node) == true)
            return false;
        
        this.map.set(`${node.ip}:${node.port}`, node);
        return true;
    }
    exists(node){
        return this.map.has(`${node.ip}:${node.port}`);
    }

    /**
     * NodeGroup iterates the list and pings the nodes.
     * Remove those who dont respond
     */
    checkGroupHealth(){
        const axios = require('axios')
        this.map.forEach(node => {
            axios.get(
                `http://${node.ip}:${node.port}/isAlive`
            )
            .then(res => {
                console.log(`(${node.ip}:${node.port}) ✅`);
            })
            .catch(error => {
                // console.log(error);
                console.log(`(${node.ip}:${node.port}) RIP 💀`);
                this.map.delete(`${node.ip}:${node.port}`);
            })
        });
    }

    // get random item from a Set
    getRandomNode() {
        let items = Array.from(this.map.keys());
        return this.map.get(items[Math.floor(Math.random() * items.length)]);
    }
}

module.exports = new NodeGroup();