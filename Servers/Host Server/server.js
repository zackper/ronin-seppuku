const express = require("express");
const app = express();
const config = require('./config');
require('./communicationManager');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// EXPRESS
const playerRoutes = require('./routes/playerRoutes.js');

app.use(express.json());
app.get("/", (req, res) => {
  res.send("This is the main server for the Ronin Seppuku platform.");
});
app.get("/isAlive", (req, res) => {
  res.send("ACK");
});

// Start listening to players at specified port
function listenToPlayers(){
  app.use('/player', playerRoutes);
  app.listen(config.hostServerPort, `${config.myIp}`, () => {
    console.log(`Express listening at http://${config.myIp}:${config.hostServerPort}`);
  });
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


// Register self

async function register(){
  const axios = require('axios');
  const { hostServerMaxPort, hostServerMinPort } = require("./config");
  const targetPort = Math.floor(Math.random() * (hostServerMaxPort - hostServerMinPort) + hostServerMinPort);

  console.log(config.mainServerUrl);
  axios
    .post(
      `${config.mainServerUrl}/host/register`,
      {
        message: "This is a carried message for host->main",
        port: targetPort
      },
      {
        headers: {
          'content-type': 'application/json',
        }
      }
    )
    .then(res => {
      console.log(`(${res.status}) ${res.data}`)
      console.log(`Target port: ${targetPort} was ${res.status}`);
      switch(res.status){
        case 200:
          config.hostServerPort = targetPort;
          listenToPlayers();
          break;
        case 500:
          register();
          break;
        default:
          break;
      }
    })
    .catch(error => {
      console.error(error)
    })
}

register();