const ip = require("ip");

let config = {
    myIp: ip.address(),
    lobbyPlayerCount: 1,

    mainServerIp: '192.168.56.1',
    mainServerPort: 30010,

    hostServerMinPort: 42000,
    hostServerMaxPort: 42500,   

    hostServerPort: -1,
    playerServerPort: 30030,

    webSocketPort: 30090,
    udpPlayerPort: 30080,
    udpServerPort: 30070,
}
config.mainServerUrl = `http://${config.mainServerIp}:${config.mainServerPort}`;

module.exports = config;