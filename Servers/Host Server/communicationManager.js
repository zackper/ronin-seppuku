const dgram = require("dgram");
const { udpServerPort, mainServerUrl, playerServerPort } = require("./config");
var players = require("./nodeLists")
const axios = require("axios");

class CommunicationManager {
  constructor() {
    // For frontend-backend comm
    this.encoder = new TextEncoder("utf-8");
    this.decoder = new TextDecoder("utf-8");

    const server = dgram.createSocket('udp4');

    server.on('error', (err) => {
      console.log(`server error:\n${err.stack}`);
      server.close();
    });
    server.on('message', (msg, rinfo) => {
      // this works ;)
      // console.log(`UDP Received: ${msg} from ${rinfo.address}:${rinfo.port}`);
    });
    server.on('listening', () => {
      const address = server.address();
      console.log(`UDP Server listening ${address.address}:${address.port}`);
    });

    server.bind(udpServerPort);
    // Prints: server listening 0.0.0.0:41234
  }


  // REST API to Main Server
  sendLobbyUpdateMessage = () => {
    // Create lobby object
    let lobby = {};
    players.map.forEach((value, key) => {
      lobby[key] = value;
    })
  
    axios
      .post(`${mainServerUrl}/host/lobby`,
        {
          lobby: lobby
        }
      )
      .then((res) => {
        console.log(`(${res.status}) ${res.data}`);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // REST API to Players
  sendLobbyStart = () => {
    console.log("LOBBY START");
    let lobby = {};
    players.map.forEach((value, key) => {
      lobby[key] = value;
    })
    console.log(lobby);

    for (const [key, value] of Object.entries(lobby)) {
      console.log(`${key}: ${value}`);
      console.log(`http://${key}:${playerServerPort}/lobby/start`);
      axios
        .post(
          `http://${key}/lobby/start`,
          {
            lobby: lobby
          }
        )
        .then((res) => {console.log(res.status)})
        .catch((error) => {console.log("Error:( ", error)});
    }
  }
}

module.exports = new CommunicationManager();