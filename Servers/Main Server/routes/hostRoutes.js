var app = require('express');
var router = app.Router();

// Singleton to server group
var hostMap = require("../hostMap");

router.get('/', (req, res, next) => {
    res.send("Server Route");
});

router.post('/register', (req, res, next) => {
    var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
    var port = req.body.port; // This reffers to the target port for the host server to listen to.

    let host = {
        ip: ip,
        port: port,
        isPlaying: false,
        lobby: []
    }

    if(hostMap.addNode(host) == true){
        console.log("New Host available 🎃")
        res.send("200"); // Registered
    }
    else{
        console.log("Port already taken");
        res.send("500"); // Port already in use
    }
});

router.post("/lobby", (req, res, next) => {
    var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;

    let host = hostMap.map.get(ip);
    host.lobby = req.body.lobby;
    console.log(host);

    res.send("Thank you for the lobby update");
})

module.exports = router;