var app = require('express');
var router = app.Router();

const hostMap = require("../hostMap");

router.get('/', (req, res, next) => {
    res.send("Clint Route");
});

router.post('/register', (req, res, next) => {
    let randomHost = hostMap.getRandomNode();
    res.send(randomHost);
});

module.exports = router;