const ip = require("ip");
console.log(ip.address());

let config = {
    myIp: ip.address(),
    lobbyPlayerCount: 2,

    mainServerIp: ip.address(),
    mainServerPort: '30010',

    hostServerPort: '30020',
    playerServerPort: '30030',

    webSocketPort: '30090',
    udpPlayerPort: '30080',
    udpServerPort: '30070',
}

config.mainServerUrl = `http://${config.mainServerIp}:${config.mainServerPort}`;

console.log("Main server URL:\n", config.mainServerUrl);

module.exports = config;