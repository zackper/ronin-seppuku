const express = require("express");
const { mainServerIp, mainServerPort } = require("./config");
const app = express();

const hostRoutes = require('./routes/hostRoutes.js');
const playerRoutes = require('./routes/playerRoutes.js');

app.get("/", (req, res) => {
  res.send("This is the main server for the Ronin Seppuku platform.");
});

app.use(express.json());
app.use('/host', hostRoutes);
app.use('/player', playerRoutes);
const port = mainServerPort;
app.listen(port, mainServerIp, () => {
  console.log(`Express listening at http://${mainServerIp}:${port}`);
});