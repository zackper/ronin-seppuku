const { default: axios } = require("axios");

/**
 * Each node should have atleast .ip && .port
 */
class NodeGroup {
    constructor() {
        console.log('NodeSingleton Instance created 🎈');
        this.map  = new Map();

        setInterval(() => {
            this.checkGroupHealth();
        }, 100)
    }

    addNode(node){
        if(this.exists(node) == true)
            return false;
        
        this.map.set(`${node.ip}:${node.port}`, node);
        return true;
    }
    exists(node){
        // Check if ip is already bounded
        this.map.forEach(iterator => {
            if (iterator.port == node.port){
                return false;
            }
        })
        return this.map.has(node.ip);
    }

    /**
     * NodeGroup iterates the list and pings the nodes.
     * Remove those who dont respond
     */
    checkGroupHealth(){
        const axios = require('axios')
        // console.log("~~~~~ Health Check ~~~~~")
        this.map.forEach(node => {
            axios.get(
                `http://${node.ip}:${node.port}/isAlive`
            )
            .then(res => {
                // console.log(`(${node.ip}:${node.port}) ✅`);
            })
            .catch(error => {
                // console.log(error);
                console.log(`(${node.ip}:${node.port}) RIP 💀`);
                this.map.delete(`${node.ip}:${node.port}`);
            })
        });
    }

    // get random item from a Set
    getRandomNode() {
        let items = Array.from(this.map.keys());
        return this.map.get(items[Math.floor(Math.random() * items.length)]);
    }
}

module.exports = new NodeGroup();