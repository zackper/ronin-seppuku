import { Input } from '@angular/core';
import { Entity, Position } from './entity';
import { environment } from "../environments/environment"

export class Player extends Entity {
  public hp: number = 100;
  public speed: number = 3;
  public range: number = 100;

  constructor() {
    super(0, 0);
    this.radius = 50;
  }

  draw(ctx: CanvasRenderingContext2D, offsets: Position = {x: 0, y: 0}) {
    ctx.beginPath();
    ctx.fillStyle = 'rgb(50, 116, 250, 100)';
    ctx.fillRect(
      this.position.x + offsets.x - this.radius / 2,
      this.position.y + offsets.y - this.radius / 2,
      this.radius,
      this.radius
    );
  }
}
