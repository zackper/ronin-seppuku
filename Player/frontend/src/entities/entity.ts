export class Entity {
  public position: Position;
  public radius: number;

  constructor(x: number, y: number) {
    this.position = new Position(x, y);
    this.radius = 30;
  }

  public draw(ctx: CanvasRenderingContext2D, offsets: Position) {

  }
}

export class Position {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
