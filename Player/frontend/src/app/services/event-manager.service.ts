import { Injectable } from '@angular/core';
import { Position } from 'src/entities/entity';
import { Player } from 'src/entities/player';
import { environment } from 'src/environments/environment';
import { InputManagerService } from './input-manager.service';
import { PlayerHolderService } from './player-holder.service';
import { SwordAttack, WorldService } from './world.service';
import { WsBridgeService, WsMessage } from './ws-bridge.service';

@Injectable({
  providedIn: 'root'
})
export class EventManagerService {
  constructor(
    private inputManager: InputManagerService,
    private worldService: WorldService,
    private wsBridge: WsBridgeService,
    private playerHolder: PlayerHolderService
  ){}

  public HandleInputManagerEvents(){
    this.findNewPosition(this.playerHolder.self);
    this.wsBridge.send({
      type: "move",
      position: this.playerHolder.self.position
    });

    // If I am attacking:
    if(this.inputManager.isAttacking == true){
      let attack: SwordAttack = {
        position: new Position(this.playerHolder.self.position.x, this.playerHolder.self.position.y),
        range: this.playerHolder.self.range,
        isHostile: false,
        timeToLive: 5
      }
      this.worldService.attackStack.push(attack);

      // Now, set attack into cooldown
      this.inputManager.isAttacking = false;
      this.inputManager.attackCd.lastTime = new Date().getTime();
      this.wsBridge.send({
        type: "attack",
        position: this.playerHolder.self.position,
        range: this.playerHolder.self.range
      })
    }
  }

  public HandleWsEvent(message: WsMessage){
    if(message.type == "update player"){
      // console.log(message.hp);
      if(environment.ip == message.ip){
        this.playerHolder.self.position = message.position;
        this.playerHolder.self.hp = message.hp;
        return;
      }

      if(this.playerHolder.enemies.has(message.ip)){
        this.playerHolder.enemies.get(message.ip).position = message.position;    
        this.playerHolder.enemies.get(message.ip).hp = message.hp;
      }
      else{
        this.playerHolder.enemies.set(message.ip, new Player());
        this.playerHolder.enemies.get(message.ip).position = message.position;    
      }
    }
    // if(message.type == "move"){
    //   if(this.playerHolder.enemies.has(message.ip)){
    //     this.playerHolder.enemies.get(message.ip).position = message.position;
    //   }
    //   else
    //     this.playerHolder.enemies.set(message.ip, new Player());
    //     this.playerHolder.enemies.get(message.ip).position = message.position;
    // }
    // else if(message.type == "attack"){
    //   //do stuff to handle incoming attacks
    // }
  }

  findNewPosition(self: Player){
    if(this.inputManager.isMoving.up){
      self.position.y -= self.speed;
    }
    if(this.inputManager.isMoving.down){
      self.position.y += self.speed;
    }
    if(this.inputManager.isMoving.left){
      self.position.x -= self.speed;
    }
    if(this.inputManager.isMoving.right){
      self.position.x += self.speed;
    }
  }

  RenderAttack(){

  }
}