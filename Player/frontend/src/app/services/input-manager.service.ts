import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InputManagerService {
  public isMoving: {
    up: boolean;
    down: boolean;
    left: boolean;
    right: boolean;
  };

  isAttacking: boolean;
  public attackCd: {
    cooldown: number;
    lastTime: number;
  }
  

  constructor() {
    this.isMoving = {
      up: false,
      down: false,
      left: false,
      right: false
    };
    this.isAttacking = false;
    this.attackCd = {
      cooldown: 2000,
      lastTime: new Date().getTime()
    }
  }

  handleKeyPress(event: KeyboardEvent) {
    let currentTime = new Date().getTime();
    switch (event.key) {
      case 'w':
        this.isMoving.up = true;
        break;
      case 's':
        this.isMoving.down = true;
        break;
      case 'a':
        this.isMoving.left = true;
        break;
      case 'd':
        this.isMoving.right = true;
        break;
      case "space":
        if(this.attackCd.lastTime < currentTime + this.attackCd.cooldown){
          this.isAttacking = true;
        }
        break;
    }
  }
  handleKeyUp(event: KeyboardEvent) {
    switch (event.key) {
      case 'w':
        this.isMoving.up = false;
        break;
      case 's':
        this.isMoving.down = false;
        break;
      case 'a':
        this.isMoving.left = false;
        break;
      case 'd':
        this.isMoving.right = false;
        break;
      case " ":
        if(this.attackCd.lastTime + this.attackCd.cooldown < new Date().getTime()){
          this.isAttacking = true;
        }
        break;
    }
  }
}

