import { Injectable } from '@angular/core';
import { env } from 'process';
import { Entity, Position } from 'src/entities/entity';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorldService {
  private width: number;
  private height: number;
  
  private grass: Grass[] = [];
  private grassCount = environment.world.grassCount;

  public attackStack: SwordAttack[];


  constructor() {
    this.width = environment.world.size;
    this.height = environment.world.size;
    this.attackStack = [];
    this.initializeGrass();
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  private initializeGrass() {
    for (let i = 0; i < this.grassCount; i++) {
      let position: Position = new Position(
        Math.random() * this.width,
        Math.random() * this.height
      );
      this.grass.push(new Grass(position.x, position.y));
    }
  }

  public draw(ctx: CanvasRenderingContext2D, offsets: Position = {x: 0, y: 0}) {
    ctx.beginPath();
    ctx.fillStyle = 'rgb(108, 116, 83)';
    ctx.fillRect(offsets.x, offsets.y, this.width, this.height);

    // Draw enviroment stuff
    this.grass.forEach((el: Grass) => {
      el.draw(ctx, offsets);
    });

    // Draw potential attacks
    for(let i = this.attackStack.length - 1; i >= 0; i--){
      let attack = this.attackStack[i];
      
      ctx.beginPath();
      if(attack.isHostile == false)
        ctx.fillStyle = "rgb(70, 30, 30, 0.5)";
      else
        ctx.fillStyle = "rgb(255, 50, 50, 0.5)";
      // ctx.arc(attack.x + offsets.x, attack.y + offsets.y, attackRange, 0, 2*Math.PI);
      ctx.arc(environment.canvasSize/2, environment.canvasSize/2, attack.range, 0, 2*Math.PI);
      ctx.fill();

      if(--attack.timeToLive == 0)
        this.attackStack.splice(i, 1);
    }
  }
}


class Grass extends Entity {
  constructor(x: number, y: number) {
    super(x, y);
    this.radius = 10;
  }

  public draw(ctx: CanvasRenderingContext2D, offsets: Position = {x: 0, y: 0}) {
    ctx.beginPath();
    ctx.fillStyle = 'green';
    ctx.fillRect(
      this.position.x + offsets.x,
      this.position.y + offsets.y,
      this.radius,
      this.radius / 2
    );
  }
}

export interface SwordAttack{
  position: Position;
  range: number;
  isHostile: boolean;
  timeToLive: number;
}