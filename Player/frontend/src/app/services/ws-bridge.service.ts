import { Injectable } from '@angular/core';
import { Position } from 'src/entities/entity';

@Injectable({
  providedIn: 'root'
})
export class WsBridgeService {
  private socket: WebSocket;
  private encoder: TextEncoder;
  private decoder: TextDecoder;

  public OnReceiveCallbacks = [];

  public send(obj: any){
    if(this.socket != undefined && this.socket.readyState == 1){
      console.log("Try to send: ", obj);
      // let buffer = this.encoder.encode(JSON.stringify(obj));
      // this.socket.send(buffer);
    }
  }

  private startWebSocket(){
    // Create WebSocket connection.
    this.socket = new WebSocket('ws://localhost:30090');
    this.socket.binaryType = 'arraybuffer';

    // Connection opened
    this.socket.addEventListener('open', (event) => {
      console.log("Connected to WS");
    });
    this.socket.addEventListener("close", (event) => {
      console.log("Socket closed");
    })

    // Listen for messages
    this.socket.addEventListener('message', (event) => {
      // Convert event data to obj
      let message = JSON.parse(event.data)
      this.OnReceiveCallbacks.forEach(callback => {
        callback(message);
      });
    });
    this.socket.addEventListener('error', (error) => {
      setTimeout(() => {
        this.startWebSocket();
      }, 200)
    });
  }

  constructor() {
    this.encoder = new TextEncoder();
    this.decoder = new TextDecoder("utf-8");

    // Normally this wont be showing but i will leave it for AI visualization
    const createAICallback = (message) => {
      console.log(message);
    }
    this.OnReceiveCallbacks.push(createAICallback);
  }
}

export interface WsMessage{
  ip: string,
  type: string,
  hp: number,
  position: Position,
  range: number
}
