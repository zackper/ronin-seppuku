import { TestBed } from '@angular/core/testing';

import { WsBridgeService } from './ws-bridge.service';

describe('WsBridgeService', () => {
  let service: WsBridgeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WsBridgeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
