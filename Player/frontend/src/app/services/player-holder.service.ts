import { Injectable } from '@angular/core';
import { Position } from 'src/entities/entity';
import { Player } from 'src/entities/player';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlayerHolderService {
  public self: Player;
  public enemies: Map<string, Player> = new Map();

  constructor() { }

  public draw(ctx: CanvasRenderingContext2D, offset: Position){
    // console.log("nani");
    // DRAW OTHER.playerHolder.enemies
    for(const [key, value] of this.enemies){
      value.draw(
        ctx,
        offset
      );
    }
    // DRAW SELF
    this.self.draw(
      ctx,
      {
        x: environment.canvasSize / 2 - this.self.position.x,
        y: environment.canvasSize / 2 - this.self.position.y,
      }
    );
  }
}
