import { Injectable } from '@angular/core';
import { Position } from 'src/entities/entity';
import { Player } from 'src/entities/player';
import { InputManagerService } from './input-manager.service';
import { PlayerHolderService } from './player-holder.service';

interface UIBar{
  yOffset: number,
  width: number,
  height: number,
  color: string
}

@Injectable({
  providedIn: 'root'
})
export class UserInterfaceService {
  constructor(
    private playerHolder: PlayerHolderService,
    private inputManager: InputManagerService
  ) { }

  private hpBar: UIBar = {
    yOffset: -70,
    width: 100,
    height: 10,
    color: "rgb(150, 0, 0)"
  }
  private cdBar: UIBar = {
    yOffset: -60,
    width: 100,
    height: 10,
    color: "rgb(50, 50 ,50)"
  }

  public draw(ctx: CanvasRenderingContext2D, offset: Position){
    this.drawHPs(ctx, offset);
    this.drawSelfCooldown(ctx, offset);
  }

  drawSelfCooldown(ctx: CanvasRenderingContext2D, offset: Position){
    let attackCd = this.inputManager.attackCd;
    let cdPercent = Math.min((new Date().getTime() - attackCd.lastTime) / attackCd.cooldown, 1);
    let position: Position = {
      x: this.playerHolder.self.position.x + offset.x,
      y: this.playerHolder.self.position.y + offset.y
    }
    this.drawBar(ctx, position, this.cdBar, cdPercent);
  }

  drawHPs(ctx: CanvasRenderingContext2D, offset: Position){
    this.drawHP(ctx, offset, this.playerHolder.self);
    for(const [key, value] of this.playerHolder.enemies){
      this.drawHP(ctx, offset, value);
    }
  }
  drawHP(ctx: CanvasRenderingContext2D, offset: Position, player: Player){    
    let position: Position = {
      x: player.position.x + offset.x,
      y: player.position.y + offset.y
    }
    this.drawBar(ctx, position, this.hpBar, Math.max(player.hp/100, 0));
  }

  private drawBar(ctx: CanvasRenderingContext2D, position: Position, bar: UIBar, percent: number){
    // Draw the exterior of the hp bar
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.strokeStyle = "rgb(0, 0, 0)"
    ctx.rect(
      position.x - bar.width/2,
      position.y + bar.yOffset,
      bar.width,
      bar.height
    );
    ctx.stroke();
    ctx.lineWidth = 1;

    // Draw the interior of the hp bar
    ctx.beginPath();
    ctx.fillStyle = bar.color;
    ctx.fillRect(
      position.x - bar.width/2,
      position.y + bar.yOffset,
      bar.width*percent,
      bar.height
    )
  }
}
