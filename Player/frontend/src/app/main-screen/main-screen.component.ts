import { Component, HostListener, OnInit } from '@angular/core';
import { off } from 'process';
import { Entity, Position } from 'src/entities/entity';
import { Player } from 'src/entities/player';
import { EncodeIntoResult } from 'util';
import { environment } from '../../environments/environment';
import { EventManagerService } from '../services/event-manager.service';
import { InputManagerService } from '../services/input-manager.service';
import { PlayerHolderService } from '../services/player-holder.service';
import { UserInterfaceService } from '../services/user-interface.service';
import { WorldService } from '../services/world.service';
import { WsBridgeService, WsMessage } from '../services/ws-bridge.service';

@Component({
  selector: 'main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss'],
})
export class MainScreenComponent implements OnInit {
  private ctx: CanvasRenderingContext2D;

  constructor(
    public playerHolder: PlayerHolderService,
    public wsBridge: WsBridgeService,
    public world: WorldService,
    public inputManager: InputManagerService,
    public eventManager: EventManagerService,
    public userInterface: UserInterfaceService
  ) {}

  ngOnInit(): void {
    // Set Callback for OnWsBridgeReceive
    this.wsBridge.OnReceiveCallbacks.push((message: WsMessage) => {
      this.eventManager.HandleWsEvent(message);
    })

    var canvas: HTMLCanvasElement = document.getElementById('tutorial') as HTMLCanvasElement;
    canvas.width = environment.canvasSize;
    canvas.height = environment.canvasSize;
    this.ctx = canvas.getContext('2d');

    this.playerHolder.self = new Player();
    this.playerHolder.self.range = 100;

    // Run this at around 30 fps
    setInterval(() => {
      this.main();
    }, 1000 / environment.fps);
  }

  main() {
    // Receive Input Events and calculate stuff
    this.eventManager.HandleInputManagerEvents(); // First handle events

    // Call all draw functions
    this.draw();
  }

  private draw(){
    this.ctx.clearRect(0, 0, environment.canvasSize, environment.canvasSize); // clear canvas
    
    let worldOffset = {
      x: environment.canvasSize/2 - this.playerHolder.self.position.x,
      y: environment.canvasSize/2 - this.playerHolder.self.position.y
    }
    
    // DRAW WORLD
    this.world.draw(this.ctx, worldOffset);
    this.playerHolder.draw(this.ctx, worldOffset);

    this.userInterface.draw(this.ctx, worldOffset);
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyPress(event: KeyboardEvent) {
    this.inputManager.handleKeyPress(event);
  }
  @HostListener('document:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    this.inputManager.handleKeyUp(event);
  }
}

