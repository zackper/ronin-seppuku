import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { InputManagerService } from './services/input-manager.service';
import { EventManagerService } from './services/event-manager.service';
import { PlayerHolderService } from './services/player-holder.service';
import { WsBridgeService } from './services/ws-bridge.service';
import { WorldService } from './services/world.service';

@NgModule({
  declarations: [
    AppComponent,
    MainScreenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [WsBridgeService, PlayerHolderService, , WorldService, InputManagerService, EventManagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
