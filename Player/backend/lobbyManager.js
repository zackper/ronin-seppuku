const axios = require("axios");
const config = require("./config");
const { mainServerUrl, hostServerPort, playerServerMinPort, playerServerMaxPort } = require("./config");

class LobbyManager {
  constructor() {
    this.host = undefined;
    this.lobby = {};
  }

  async GetHostData() {
    // Connect to main server and then connect to host
    let host;
    console.log("main server ip :" ,mainServerUrl);
    await axios
      .post(`${mainServerUrl}/player/register`)
      .then((res) => {
        host = res.data;
      })
      .catch((error) => {
        console.error(error.data);
      });
    return host;
  }
  async ConnectToHost() {
    let host = await this.GetHostData();
    console.log("Host data:", host);
    if (host == undefined) {
      console.log("Main Server returned 0 available servers 🙈");
      return false;
    }

    // Find random ip for self
    const targetPort = Math.floor(Math.random() * (playerServerMaxPort - playerServerMinPort) + playerServerMinPort);

    await axios
      .post(`http://${host.ip}:${host.port}/player/register`, {port: targetPort})
      .then(async (res) => {
        switch(res.status){
          case 200:
            console.log("200");
            config.playerServerPort = targetPort;
            this.host = host;
            break;
          case 500: 
            await this.ConnectToHost();
            break;
          default:
            console.log("this is not happening");
            break;
        }
      })
      .catch((error) => {
        console.log("Error: ", error);
      });
  }
}

module.exports = new LobbyManager();
