var lobbyManager = require("./lobbyManager");
const config = require("./config");
const communicationManager = require("./communicationManager");
const { isBot } = require("./config");
require("./communicationManager");

main = async () => {
	communicationManager.InitWSServer();
	
	await lobbyManager.ConnectToHost();
	var appPromise = require('./app').appPromise;
	var http = require('http');
	const port = '3000';
	appPromise.then(function(app) {
		console.log("Resolved app promise")
		app.set('port', port);
		var server = http.createServer(app);
		server.listen(port, () => {
			console.log("Started express server");
		});
	}).catch(rejects => {
		console.log("Error on app promise");
	});


	if(isBot == true){
		while(true){
			AgentAI();
		}
	}
}

function AgentAI(){
	// console.log(gamestateManager.gamestate)
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Start of basic stuff

main();