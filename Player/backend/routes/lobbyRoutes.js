var app = require("express");
var router = app.Router();
const config = require("../config");
const axios = require("axios");

router.get("/", (req, res) => {
    res.send("die");
});
router.get("/isAlive", (req, res) => {
    res.send("ACK");
});
router.post("/lobby/start", (req, res) => {
    console.log("Start game...")
    console.log("Lobby List: ", req.body.lobby);
    lobbyManager.lobby = req.body.lobby;
    gamestateManager.Initialize(lobbyManager.lobby);
    // communicationManager.InitUDPServer();
    // communicationManager.InitWSServer();
    
    res.send("(200) Starting now...");
})

module.exports = router;
