const { default: axios } = require("axios");

/**
 * Each node should have atleast .ip && .port
 */
class NodeMap {
    constructor() {
        console.log('NodeSingleton Instance created 🎈');
        this.map  = new Map();

        setInterval(() => {
            this.checkGroupHealth();
        }, 5000)
    }

    addNode(node){
        if(this.exists(node) == true)
            return false;
        
        this.map.set(node.ip, node);
        return true;
    }
    exists(node){
        return this.map.has(node.ip);
    }

    /**
     * NodeGroup iterates the map and pings the nodes.
     * Remove those who dont respond
     */
    checkGroupHealth(){
        const axios = require('axios')
        // console.log("~~~~~ Health Check ~~~~~")
        this.map.forEach(node => {
            axios.get(
                `http://${node.ip}:30020/isAlive`
            )
            .then(res => {
                // console.log(`(${node.ip}) ✅`);
            })
            .catch(error => {
                // console.log(error);
                console.log(`(${node.ip}) RIP 💀`);
                this.map.delete(node.ip);
            })
        });
    }
}

module.exports = new NodeMap();