/**
 * Receives updates from:
 *  - frontend with Ws
 *  - other players with udp multicast
 *  - true state corrections from host
 * 
 * This class's job is to handle all those sources
 * and maintain a correct gamestate. Then push all
 * changes to render in the frontend
 */

const config = require("../config");
const { myIp } = require("../config");

class GamestateManager{   
    constructor(){
        this.gamestate = {};
    }

    // For starters send everything to everyone
    Initialize(lobby){
        for (const [key, value] of Object.entries(lobby)) {
            this.gamestate[key] = value;
        }
    }

    patchGamestate(message){
        if(this.gamestate[message.ip] == undefined)
            this.gamestate[message.ip] = {};

        if(message.type == "move")
            this.patchMovementMessage(message);
        else if(message.type == "attack")
            this.patchAttackMessage(message);
        else if(message.type == "roleback")
            this.gamestate = message;

        // console.log(this.gamestate);
    }

    patchMovementMessage(message){
        this.gamestate[message.ip].position = message.position;
        if(message.ip != myIp)
            this.wsUpdatePlayer(this.gamestate[message.ip]);
    }
    patchAttackMessage(message){
        for (const [key, player] of Object.entries(this.gamestate)) {
            if(`${message.ip}:${message.port} == ${myIp}:${config.playerServerPort}`) // Dont attack self
                continue;
            if(this.distance(player.position, message.position) < message.range){
                player.hp -= 40;
                console.log(`player ${player.ip} got damaged by ${message.ip}. Hp left: ${player.hp}`);
                this.wsUpdatePlayer(player);
            }
        }
        
    }

    wsUpdatePlayer(playerState){
        const communicationManager = require("../communicationManager");
        let message = playerState;
        message.type = "update player";
        communicationManager.wsSend(message);
    }

    distance(pos1, pos2){
        return Math.sqrt(Math.pow(pos1.x - pos2.x, 2) + Math.pow(pos1.y - pos2.y, 2));
    }
}

module.exports = new GamestateManager();