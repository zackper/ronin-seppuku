const WebSocket = require("ws");
var dgram = require("dgram");
const { udpPlayerPort, udpServerPort, webSocketPort, myIp, hostServerPort, playerServerPort } = require("./config");
var lobbyManager = require("./lobbyManager");
const eventManager = require("./eventManager");
const config = require("./config");


class CommunicationManager {
  constructor() {
    // this.host = "localhost";
    // For frontend-backend comm
    this.socket = null;
    this.encoder = new TextEncoder("utf-8");
    this.decoder = new TextDecoder("utf-8");
  }

  InitWSServer(){
    if(config.isBot == false)
      return;

    this.wsServer = new WebSocket.Server({ port: webSocketPort });
    // This part is for internal backend-frontend communication
    // Position and attacks are firstly received here. Send to all other receivers!
    this.wsServer.on("connection", (wsSocket) => {
      this.wsSocket = wsSocket;
      wsSocket.binaryType = "arraybuffer";
      wsSocket.on("message", (decodedMessage) => {
        var messageString = this.decoder.decode(decodedMessage);
        var message = JSON.parse(messageString);
        eventManager.handleWsMessage(message);
      });
    });
  
  }

  InitUDPServer(){
    const config = require("./config");
    console.log("PLAYER SERVER PORT: ", config.playerServerPort);

    // For this.host - main server comm
    this.udpServer = dgram.createSocket("udp4");
    this.udpServer.on("message", (messageBuffer, info) => {
      let messageString = this.decoder.decode(messageBuffer);
      let message = JSON.parse(messageString);
      eventManager.handleUdpMessage(message);
    })
    this.udpServer.bind(config.playerServerPort);
  }

  wsSend(message){
    if(this.wsSocket != undefined)
      this.wsSocket.send(JSON.stringify(message));
  }

  /**
   * 
   * @param {message to send} message 
   * @param {port of receiver} port 
   * @param {ip of receiver} ip 
   */
  udpSend(message, port, ip){
    const buffer = Buffer.from(message);
    this.udpServer.send(buffer, port, ip, (error) => {
      if(error)
        console.log("Error: ", error);
    });
  }

  /**
   * This function is used to send a message to the rest of the lobby
   * @param {attack or move type message} message 
   */
  udpSendToLobby(message){
    let lobby = lobbyManager.lobby;
    for (const [key, player] of Object.entries(lobby)) {
      if(key == `${myIp}:${config.playerServerPort}`){
        continue;
      }
      
      this.udpSend(JSON.stringify(message), player.port, player.ip);
    }
    this.udpSend(JSON.stringify(message), lobbyManager.host.port, lobbyManager.host.ip);
  }
}

module.exports = new CommunicationManager();