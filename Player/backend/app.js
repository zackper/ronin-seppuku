const express = require("express");
const app = express();

const lobbyRoutes = require("./routes/lobbyRoutes");
app.use(express.json());

exports.appPromise = new Promise(function(resolve, reject) {
    startTheAsyncOperation(function(err, result) {
        if (err) {
            reject(err);
            return;
        }
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'jade');
        app.use(express.static(path.join(__dirname, 'public')));
        app.use('/', routes);
        resolve(app);
    });
});