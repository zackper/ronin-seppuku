const config = require("./config");
const { myIp } = require("./config");
const gamestateManager = require('./gamestate/gamestateManager');

class EventManager{
    constructor(){
    }

    handleWsMessage(message){
        const communicationManager = require("./communicationManager");
        // Update my own gamestate according to the message
        message.ip = myIp;
        message.port = config.playerServerPort;
        // gamestateManager.patchGamestate(message);

        /*
        gamestateManager.patchGamestate(message);

        // Send udp packages to everyone who should receive it
        communicationManager.udpSendToLobby(message);
        */
    }

    handleUdpMessage(message){
        const communicationManager = require("./communicationManager");
        // Update my own gamestate according to the message
        let collision = gamestateManager.patchGamestate(message);
        // collision = false;
        // if(collision == true){
        //     // If it collides with my gamestate ignore the message
        // }
        // else{
        //     // It doesnt collide with my gamestate so render it
        //     communicationManager.wsSend(message);
        // }
    }
}

module.exports = new EventManager();