const ip = require("ip");

class Config{
    constructor(){
        this.myIp = ip.address();
        this.lobbyPlayerCount = 2;

        this.mainServerIp = '192.168.56.1';
        this.mainServerPort = 30010;

        this.hostServerMinPort = 42000;
        this.hostServerMaxPort = 42500;
        this.playerServerMinPort = 42501;
        this.playerServerMaxPort = 43000;

        this.hostServerPort = -1;
        this.playerServerPort = -1;

        this.webSocketPort = 30090;

        this.mainServerUrl = `http://${this.mainServerIp}:${this.mainServerPort}`;

        var args = process.argv.splice(2);
        if(args == "player")
            this.isBot = false;
        else
            this.isBot = true;
        console.log("this is bot :" , this.isBot);
    }
}

module.exports = new Config();